import 'reflect-metadata';
import { ApolloServer } from 'apollo-server';
import { buildSchema } from 'type-graphql';
import * as mongoose from 'mongoose';

import { UserResolver, CaseResolver } from './app/graphql/resolvers';

const { MONGO_USER, MONGO_PASS } = process.env;

const PORT = parseInt(process.env.PORT || `3090`, 10);

(async () => {
  const schema = await buildSchema({
    resolvers: [UserResolver, CaseResolver],
  });

  const server = new ApolloServer({
    schema,
    context: ({ req }) => {
      const token: string = req.headers.authorization || ``;

      token && console.log(token);
      return { token };
    },
  });

  await mongoose
    .connect(`mongodb+srv://mago.5uqna.mongodb.net/abds`, {
      auth: {
        username: MONGO_USER,
        password: MONGO_PASS,
      },
      w: `majority`,
      retryWrites: true,
    })
    .then(() => console.log(`connect mongo ok 📦`));

  await server.listen(PORT, () => {
    console.log(`> Apollo Server Listen on: http://localhost:${PORT} 🛸`);
  });
})();

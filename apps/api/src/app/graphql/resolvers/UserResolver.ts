import * as crypto from 'crypto';

import { Resolver, Query, Mutation, Arg, ID } from 'type-graphql';
import * as argon2 from 'argon2';

import UserType, { CreateUserInput, UpdateUserInput } from '../types/UserType';
import UserSchema from '../../database/schemas/UserSchema';

@Resolver()
class UserResolver {
  @Query(() => [UserType])
  async getAllUsers() {
    const users = await UserSchema.find();

    return users;
  }

  @Query(() => UserType)
  async getUser(@Arg(`id`) id: string) {
    const user = await UserSchema.findById(id);

    return user;
  }

  @Mutation(() => ID, { nullable: true })
  async createUser(@Arg(`create_option`) createOption: CreateUserInput) {
    const hasUserWithEmailOrUsername: boolean = await UserSchema.exists({
      email: createOption.email,
      username: createOption.username,
    });

    const userPassword = await argon2.hash(createOption.password, {
      salt: crypto.randomBytes(1024),
      type: argon2.argon2id,
    });

    if (!hasUserWithEmailOrUsername) {
      const user = await UserSchema.create({
        username: createOption.username,
        email: createOption.email,
        password: userPassword,
      });

      return user.id;
    }

    return null;
  }

  @Mutation(() => ID)
  async updateUser(
    @Arg(`id`) id: string,
    @Arg(`update_option`) updateOption: UpdateUserInput,
  ) {
    const user = await UserSchema.findByIdAndUpdate(id, updateOption);

    return user.id;
  }

  @Mutation(() => ID)
  async deleteUser(@Arg(`id`) id: string) {
    const user = await UserSchema.findByIdAndDelete(id);

    return user.id;
  }
}

export default UserResolver;

import { Resolver, Mutation } from 'type-graphql';

@Resolver()
class CaseResolver {
  @Mutation(() => String)
  async createCase() {
    return `world?`;
  }
}

export default CaseResolver;

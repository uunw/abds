import { ObjectType, InputType, Field, ID } from 'type-graphql';
import { MinLength, MaxLength, IsEmail } from 'class-validator';

@ObjectType()
class UserType {
  @Field(() => ID)
  id: string;

  @Field(() => String)
  username: string;

  @Field(() => String)
  email: string;

  @Field(() => Date)
  createdAt: Date;

  @Field(() => Date)
  updatedAt: Date;
}

@InputType()
class CreateUserInput {
  @Field(() => String)
  @MinLength(3)
  @MaxLength(20)
  username: string;

  @Field(() => String)
  @IsEmail()
  email: string;

  @Field(() => String)
  @MinLength(1)
  @MaxLength(1024)
  password: string;
}

@InputType()
class UpdateUserInput {
  @Field(() => String, { nullable: true })
  username?: string;

  @Field(() => String, { nullable: true })
  email?: string;
}

export { CreateUserInput, UpdateUserInput };
export default UserType;

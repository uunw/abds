import { Document, Schema, Model, model } from 'mongoose';

interface UserType extends Document {
  id: string;
  username: string;
  email: string;
  password: string;
}

const UserSchema = new Schema<UserType>(
  {
    username: { require: true, type: String },
    email: { require: true, type: String },
    password: { require: true, type: String },
  },
  {
    collection: `users`,
    timestamps: true,
  },
);

const User: Model<UserType> = model(`User`, UserSchema);

export default User;
